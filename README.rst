=========================
The ``lino-shop`` package
=========================




Lino Shop is a customizable management system for web shops.

- Project homepage: https://gitlab.com/lino-framework/shop

- Documentation:
  https://lino-framework.gitlab.io/shop/

- For *introductions* and *commercial information*
  please see `www.saffre-rumma.net
  <https://www.saffre-rumma.net>`__.


