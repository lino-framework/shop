# -*- coding: UTF-8 -*-
# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""Plugins for Lino Shop.

.. autosummary::
   :toctree:

    shop

"""
