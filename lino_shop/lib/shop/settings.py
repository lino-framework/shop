# -*- coding: UTF-8 -*-
# Copyright 2021 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

from lino.projects.std.settings import *
from lino_shop import SETUP_INFO


class Site(Site):

    verbose_name = "Lino Shop"
    description = SETUP_INFO['description']
    version = SETUP_INFO['version']
    url = SETUP_INFO['url']

    demo_fixtures = 'std few_countries minimal_ledger \
    demo demo2 demo3 checkdata'.split()

    user_types_module = 'lino_shop.lib.shop.user_types'
    workflows_module = 'lino_shop.lib.shop.workflows'
    default_build_method = 'weasy2pdf'
    default_ui = "lino_react.react"

    def get_installed_plugins(self):
        yield super().get_installed_plugins()
        yield 'lino_shop.lib.shop'
        yield 'lino.modlib.users'
        yield 'lino.modlib.gfks'
        # yield 'lino.modlib.system'
        yield 'lino_xl.lib.countries'
        yield 'lino_xl.lib.contacts'
        #~ yield 'lino_xl.lib.households'

        yield 'lino_xl.lib.excerpts'

        # yield 'lino_xl.lib.outbox'
        yield 'lino_xl.lib.albums'
        # yield 'lino.modlib.files'
        yield 'lino.modlib.publisher'
        yield 'lino.modlib.weasyprint'
        yield 'lino.modlib.export_excel'
        # yield 'lino.modlib.tinymce'
        # yield 'lino.modlib.wkhtmltopdf'

        # accounting must come before trading because its demo fixture
        # creates journals (?)

        # yield 'lino_xl.lib.sepa'
        # yield 'lino_xl.lib.vat'
        yield 'lino_shop.lib.products'
        yield 'lino_xl.lib.trading'
        # yield 'lino_xl.lib.invoicing'
        # yield 'lino_xl.lib.accounting'
        # yield 'lino_xl.lib.finan'
        # yield 'lino_xl.lib.bevat'
        # yield 'lino_xl.lib.sheets'
        yield 'lino_xl.lib.shopping'

    def get_plugin_configs(self):
        """
        Change the default value of certain plugin settings.

        """
        yield super(Site, self).get_plugin_configs()
        yield ('countries', 'hide_region', True)
        yield ('countries', 'country_code', 'BE')
        # yield ('accounting', 'use_pcmn', True)
        yield ('products', 'menu_group', 'trading')
        yield ('users', 'allow_online_registration', True)
        # yield ('accounting', 'sales_method', 'pos')
        yield ('accounting', 'has_payment_methods', True)
        # yield ('invoicing', 'voucher_model', 'trading.CashInvoice')
        # yield ('invoicing', 'voucher_type', 'trading.CashInvoicesByJournal')
        yield ('publisher', 'locations',
               (("prod", 'products.Products'), ("things", 'products.Things'),
                ("books", 'products.Books'), ("furn", 'products.Furniture')))
