.. _shop:

===========
Lino Shop
===========

Welcome to the **Lino Shop** project homepage.

.. py2rst::

  from lino_shop import SETUP_INFO
  print(SETUP_INFO['long_description'])


.. note:: The following content, if you happen to see it, is not
          meaningful. Remove this note from your copy of
          :file:`docs/index.rst`.


Content
========

.. toctree::
   :maxdepth: 1

   install/index
   guide/index
   specs/index
   api/index
   changes
   
