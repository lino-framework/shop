.. _shop.guide:

=======================
Lino Shop User's Guide
=======================

This is not yet written.

:ref:`shop` defines the following :term:`user types <user type>`.


.. currentmodule:: lino_shop.lib.shop

.. class:: UserTypes

  .. attribute:: user

    A web shop customer. The default type after :term:`online registration`.

  .. attribute:: vendor

    Somebody who can create products.

  .. attribute:: staff

  .. attribute:: admin
