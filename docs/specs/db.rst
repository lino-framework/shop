.. doctest docs/specs/db.rst
.. _shop.specs.db:

================================
Database structure of Lino Shop
================================

This document describes the database structure.

.. contents::
  :local:


.. include:: /../docs/shared/include/tested.rst

>>> import lino
>>> lino.startup('lino_shop.projects.shop1.settings')
>>> from lino.api.doctest import *


>>> analyzer.show_db_overview()
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
33 plugins: lino, about, jinja, react, shop, printing, system, users, contenttypes, gfks, office, xl, countries, contacts, excerpts, uploads, albums, linod, bootstrap3, publisher, weasyprint, export_excel, products, memo, checkdata, periods, accounting, bevat, vat, trading, shopping, staticfiles, sessions.
50 models:
=========================== ============================ ========= =======
 Name                        Default table                #fields   #rows
--------------------------- ---------------------------- --------- -------
 accounting.Account          accounting.Accounts          18        22
 accounting.Journal          accounting.Journals          25        4
 accounting.LedgerInfo       accounting.LedgerInfoTable   2         0
 accounting.MatchRule        accounting.MatchRules        3         4
 accounting.Movement         accounting.Movements         11        0
 accounting.PaymentMethod    accounting.PaymentMethods    6         3
 accounting.PaymentTerm      accounting.PaymentTerms      12        9
 accounting.Voucher          accounting.AllVouchers       8         0
 albums.Album                albums.Albums                5         12
 bevat.Declaration           bevat.Declarations           47        0
 checkdata.Message           checkdata.Messages           6         2
 contacts.Company            contacts.Companies           27        13
 contacts.CompanyType        contacts.CompanyTypes        7         16
 contacts.Partner            contacts.Partners            24        82
 contacts.Person             contacts.Persons             31        69
 contacts.Role               contacts.Roles               4         3
 contacts.RoleType           contacts.RoleTypes           5         5
 contenttypes.ContentType    gfks.ContentTypes            3         50
 countries.Country           countries.Countries          6         10
 countries.Place             countries.Places             9         80
 excerpts.Excerpt            excerpts.Excerpts            11        0
 excerpts.ExcerptType        excerpts.ExcerptTypes        17        4
 linod.SystemTask            linod.SystemTasks            24        2
 memo.Mention                memo.Mentions                5         3
 periods.StoredPeriod        periods.StoredPeriods        7         0
 periods.StoredYear          periods.StoredYears          5         15
 products.Author             products.Authors             6         2
 products.Book               products.Books               3         3
 products.Category           products.Categories          15        7
 products.PriceRule          products.PriceRules          3         0
 products.Product            products.Products            22        10
 products.Thing              products.Things              2         4
 publisher.Page              publisher.Pages              15        18
 sessions.Session            users.Sessions               3         ...
 shopping.Address            shopping.Addresses           13        0
 shopping.Cart               shopping.Carts               7         0
 shopping.CartItem           shopping.CartItems           4         0
 shopping.DeliveryMethod     shopping.DeliveryMethods     5         3
 system.SiteConfig           system.SiteConfigs           5         1
 trading.CashInvoice         trading.CashInvoices         29        0
 trading.InvoiceItem         trading.InvoiceItems         13        0
 trading.PaperType           trading.PaperTypes           5         2
 trading.VatProductInvoice   trading.Invoices             26        0
 uploads.Upload              uploads.Uploads              13        6
 uploads.UploadType          uploads.UploadTypes          8         2
 uploads.Volume              uploads.Volumes              4         1
 users.Authority             users.Authorities            3         0
 users.User                  users.AllUsers               22        6
 vat.InvoiceItem             vat.InvoiceItemTable         9         0
 vat.VatAccountInvoice       vat.Invoices                 21        0
=========================== ============================ ========= =======
<BLANKLINE>
