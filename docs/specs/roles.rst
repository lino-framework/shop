.. doctest docs/specs/roles.rst
.. _shop.specs.roles:

========================
User roles in Lino Shop
========================

>>> import lino
>>> lino.startup('lino_shop.projects.shop1.settings')
>>> from lino.api.doctest import *


Show the list of users:

>>> rt.show(rt.models.users.AllUsers)
... #doctest: +NORMALIZE_WHITESPACE -REPORT_UDIFF
========== ===================== ============ ===========
 Username   User type             First name   Last name
---------- --------------------- ------------ -----------
 jill       500 (Staff)           Jill         Doe
 joe        100 (Customer)        Joe          Doe
 john       200 (Vendor)          John         Doe
 robin      900 (Administrator)   Robin        Rood
 rolf       900 (Administrator)   Rolf         Rompen
 romain     900 (Administrator)   Romain       Raffault
========== ===================== ============ ===========
<BLANKLINE>


Menus
-----

Site manager
------------------

Rolf is a :term:`site manager`, he has a complete menu.

>>> show_menu('robin')
... #doctest: +ELLIPSIS +NORMALIZE_WHITESPACE +REPORT_UDIFF
- Contacts : Persons, Organizations
- Office : My Excerpts, My Upload files
- Publisher : Pages
- Sales : My Addresses, Shopping cart, Sales invoices (SLS), Sales credit notes (SLC)
- Accounting :
  - Purchases : Purchase invoices (PRC)
  - VAT : VAT declarations (VAT)
  - My movements
- Reports :
  - VAT : Intra-Community purchases, Intra-Community sales
- Configure :
  - System : Users, Site configuration, System tasks
  - Places : Countries, Places
  - Contacts : Legal forms, Functions
  - Office : Excerpt Types, Library volumes, Upload types
  - Publisher : Albums, Special pages
  - Sales : Furniture, Books, Things, Services, Product Categories, Price rules, Authors, Paper types, Delivery methods
  - Accounting : Fiscal years, Accounting periods, Accounts, Journals, Payment terms, Payment methods
- Explorer :
  - System : Authorities, User types, User roles, content types, Background procedures, Data checkers, Data problem messages
  - Contacts : Contact persons, Partners
  - Office : Excerpts, Upload files, Upload areas, Mentions
  - Sales : Price factors, Sales invoices, Sales invoice items, Shopping carts, Addresses
  - Accounting : Common accounts, Match rules, Vouchers, Voucher types, Movements, Trade types, Journal groups
  - VAT : Belgian VAT declarations, Declaration fields, VAT areas, VAT regimes, VAT classes, VAT columns, Invoices, VAT rules
- Site : About, User sessions
