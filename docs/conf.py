from atelier.sphinxconf import configure

configure(globals())
from lino.sphinxcontrib import configure

configure(globals(), 'lino_shop.projects.shop1.settings')

# extensions += ['lino.sphinxcontrib.logo']
extensions += ['lino.sphinxcontrib.help_texts_extractor']
help_texts_builder_targets = {'lino_shop.': 'lino_shop.lib.shop'}

project = "Lino Shop"
copyright = '2021 Rumma & Ko Ltd'
html_title = "Lino Shop"

# html_context.update(public_url='https://lino-framework.gitlab.io/shop')

# from rstgen.sphinxconf import interproject
# interproject.configure(globals())
intersphinx_mapping['cg'] = ('https://community.lino-framework.org/', None)
intersphinx_mapping['ug'] = ('https://using.lino-framework.org/', None)
# intersphinx_mapping['hg'] = ('https://hosting.lino-framework.org/', None)
